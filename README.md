## DevOps Challenge
Welcome to the Limehome Devops challenge! Thank you in advance for your interest in our company and taking the time to solve it. 

**What we are looking for in your answers**:
1. **Basic coding skills**:
For this role you are not expected to be an expert coder. We still expect you being able to write simple scripts to automate any infrastructure tasks.
2. **Problem solving skills**:
We want to see how you approach the solving of a new problem. Make sure to walk us through your thought process while answering.
3. **Communication skills**: How well are you able to describe concepts? The more concise the better!
Brief descriptions and straight to the point answers are always appreciated.

**What you should send to us**:
1. **Coding Challenge**: Send us your code and instructions on how to run it (.zip or public repo). We will definitely try it out! In terms of languages, pick one which you think is the right fit for this job. We deeply value the simplicity of the solutions, please highlty consider this when you're resolving it.
2. **Concepts && Security**: It is sufficient to put all answers in a text file.

## Coding Challenge
**Before we start**: Please refrain from solving this problem with bash or by piping commands in your shell. While this is sometimes a valid solution, we want to see you write an actual script.

We are looking for a text file in one of our S3 buckets, but can’t seem to find it anymore. This is where you come in. Write a script which iterates through an S3 bucket and tries to find a certain substring in all of the text files. Return all file names, where the file content contains said substring.

_Input_: s3_bucket_name: string, substring: string
_Output_: Sequence of files name which contains this substring

We do not expect you to own an AWS account with an S3 Bucket, feel free to use localstack for the implementation: https://github.com/localstack/localstack
Else you can consider creating an [Amazon free account](https://aws.amazon.com/free/).

Things to think about:
* Imagine sharing this script with your coworkers. How would you include instructions on how to run it?
* Please consider sharing a production-ready code: optimal performance, clean and maintainable.
* Feel free to extend the code by adding more features, we'll be eager to check out what you've added.

## Concepts
Please explain the following concepts in your own words. Assume that we know nothing about them.
### Infrastructure as Code
What is it? Why would I want it? Are there any alternatives ?
### Observability
Please explain this term in the context of micro services. What do we want to observe? What kind of challenges do you see in a distributed environment? How can we solve them?

## Security
Imagine you would join our team and put in charge of securing our AWS infrastructure. What are the first three things that you check, to limit the risk of a breach? Explain why.
